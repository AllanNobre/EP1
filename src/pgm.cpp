#include "pgm.hpp"

//Construtor/Destrutor
PGM::PGM(){
	posicao_mensagem = 0;
	setArquivo("");
	setNumeroMagico("");
	setEndereco("");
	setLargura(0);
	setComprimento(0);
	setNivelDeCor(0);
	setTamanhoTotal(0);
	setTamanhoImagem(0);
	setTamanhoCabecalho(0);
}

PGM::PGM(string endereco) {
	posicao_mensagem = 0;
	setArquivo("");
	setNumeroMagico("");
	setEndereco(endereco);
	setLargura(0);
	setComprimento(0);
	setNivelDeCor(0);
	setTamanhoTotal(0);
	setTamanhoImagem(0);
	setTamanhoCabecalho(0);	
}

PGM::~PGM() {

}

//Acessores
void PGM::setPosicaoMensagem(int posicao_mensagem) {
	this->posicao_mensagem = posicao_mensagem;
}

int PGM::getPosicaoMensagem() {
	return posicao_mensagem;
}

//Outros
void PGM::ExtrairPosicaoMensagem() {
	string posicao_mensagem;

	for(int i=0;i<=getTamanhoTotal();i++) {
		if (getArquivo()[i] == '#') {        // Identifica um comentário que contém a posição usando o sharp
			i+=2;                            // Pula para a posição onde está a informação desejada
			while(getArquivo()[i] != ' '){
				posicao_mensagem+=getArquivo()[i];
				i++;
			}
			break;
		}
	}
	//Passando dado para o atriuto
	this->posicao_mensagem = stoi(posicao_mensagem);
}

void PGM::VerificaNumeroMagico() {          // Verifica se a imagem é realmente uma PGM
	if (getNumeroMagico() == "P5") {
		cout << "Mensagem:" << endl << endl;
	} else if (getNumeroMagico() == "P6") {
		cout << "Ops, parece que você nos deu uma imagem do tipo '.ppm' na opção da '.pgm'." << endl;
		exit(0);
	} else {
		cout << "Ops, parece que você nos deu uma imagem diferente do esperado!" << endl;
		exit(0);
	}
}

void PGM::CalculaTamanhoImagem() {
	setTamanhoImagem(getLargura()*getComprimento());
}

void PGM::DecifraImagem() {
	int inicio = 0;
	int contador_if = 0;           // Contador que determina a entrada no if
	int contador_ciclos = 0;       // Contador que define o termino de um ciclo de 8
	int encontra_sharp = 0;        // Para poder deixar o primeiro # da mensagem passar e parar no último
	char c;
	char c1;

	inicio = getTamanhoCabecalho() + getPosicaoMensagem() + 1;   // Soma que determina o local de inicio da mensagem

	for(int i=inicio;i<=getTamanhoTotal();i++) {
		contador_if++;
		if (contador_if == 1){
			c = getArquivo()[i];   // Binário qualquer -> xxxxxxxx
			c = (c & 0x01);        // Binário menos significativo -> 0000000x
			c = (c << 1);          // Binário menos significativo movido uma casa pra esquerda -> 000000x0 

			contador_ciclos++;
		} else if (contador_if == 2) {
			c1 = getArquivo()[i];  // Binário qualquer -> xxxxxxxx
			c1 = (c1 & 0x01);      // Binário menos significativo -> 0000000x
			c = (c | c1);          // Binários unidos -> 000000x0 + 0000000x = 000000xx

			contador_ciclos++;

			if (contador_ciclos < 8) {
				c = (c << 1);      // Binário movido pra esquerda -> 00000xx0
				contador_if = 1;   // Para não entrar no primeiro if
			} else {

				//Esquema para printar a mensagem do início ao ultimo char antes do sharp
				if (encontra_sharp == 0) {

				// Printa o primeiro sharp	
					encontra_sharp = 1;    // Evita a entrada nesse if novamente pois já foi printado o primeiro sharp

					cout << c;             // Printa no terminal o char extraído
					contador_if = 0;       // Zera os valores para poder reiniciar a extração
					contador_ciclos = 0;   // Zera os valores para poder reiniciar a extração
				} else {
					if (c == '#') {

				// Printa o último sharp e quebra o laço 'for' 		
						cout << endl << endl << "Fim da Mensagem!" << endl << "Adeus, Obrigado!" << endl << endl;  // Printa no terminal as quebras de linha, não printa o ultimo sharp
						break;
					} else {

				// Se não for um sharp, continua a extração normalmente
						cout << c;              // Printa no terminal o char extraído
						contador_if = 0;        // Zera os valores para poder reiniciar a extração
						contador_ciclos = 0;    // Zera os valores para poder reiniciar a extração
					}
				}
			}
		}
	}
}