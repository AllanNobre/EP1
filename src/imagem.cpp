#include "imagem.hpp"

//Construtor/Destrutor
Imagem::Imagem(){
	arquivo = "";
	numero_magico = "";
	endereco = "";
	largura = 0;
	comprimento = 0;
	nivel_de_cor = 0;
	tamanho_total = 0;
	tamanho_imagem = 0;
	tamanho_cabecalho = 0;
}

Imagem::~Imagem(){

}

//Acessores
void Imagem::setArquivo(string arquivo) {
	this->arquivo = arquivo;
}

string Imagem::getArquivo(){
	return arquivo;
}

void Imagem::setNumeroMagico(string numero_magico) {
	this->numero_magico = numero_magico;
}

string Imagem::getNumeroMagico(){
	return numero_magico;
}

void Imagem::setEndereco(string endereco) {
	this->endereco = endereco;
}

string Imagem::getEndereco(){
	return endereco;
}

void Imagem::setLargura(int largura) {
	this->largura = largura;
}

int Imagem::getLargura(){
	return largura;
}

void Imagem::setComprimento(int comprimento) {
	this->comprimento = comprimento;
}

int Imagem::getComprimento(){
	return comprimento;
}

void Imagem::setNivelDeCor(int nivel_de_cor) {
	this->nivel_de_cor = nivel_de_cor;
}

int Imagem::getNivelDeCor(){
	return nivel_de_cor;
}

void Imagem::setTamanhoTotal(int tamanho_total) {
	this->tamanho_total = tamanho_total;
}

int Imagem::getTamanhoTotal(){
	return tamanho_total;
}

void Imagem::setTamanhoImagem(int tamanho_imagem) {
	this->tamanho_imagem = tamanho_imagem;
}

int Imagem::getTamanhoImagem(){
	return tamanho_imagem;
}

void Imagem::setTamanhoCabecalho(int tamanho_cabecalho) {
	this->tamanho_cabecalho = tamanho_cabecalho;
}

int Imagem::getTamanhoCabecalho(){
	return tamanho_cabecalho;
}

//Outros
void Imagem::AbrirImagem(){
	ifstream imagem;
	string arquivo;
	int tamanho_total = 0;
	char caracter;

	imagem.open(endereco.c_str());

	//Checando caminho da imagem
	if(imagem.fail()) {
		cout << "Erro ao abrir a imagem utilizando o caminho fornecido!" << endl << endl;
		exit(0);
	}

	//Extraindo imagem para uma string e contando quantidade de chars
	while (!imagem.eof()) {
		imagem.get(caracter);
		arquivo+=caracter;
		tamanho_total++;
	}

	//Passando dados para os atributos
	this->arquivo = arquivo;
	this->tamanho_total = tamanho_total;

	//Fechando imagem
	imagem.close();
}

void Imagem::ExtrairDados(){
	string numero_magico;
	string largura;
	string comprimento;
	string nivel_de_cor;
	int contador = 0;
	int i=0; // Tamanho do cabeçalho

	//Extraindo dados
	for(i=0;i<=tamanho_total;i++) {
		contador++;

		//Ignorando comentários
		if (arquivo[i] == '#') {
			contador--;
			while(arquivo[i]!='\n') {
				i++;
			}
		} else {
		//Extraindo número mágico 
			if (contador == 1) {
				while(arquivo[i]!='\n') {
				numero_magico+=arquivo[i];
				i++; 
				}		
			} else if (contador == 2) {
				//Extraindo largura
				while(arquivo[i]!=' ') {
				largura+=arquivo[i];
				i++;
				}
				//Extraindo comprimento
				i++;	
				while(arquivo[i]!='\n') {
					comprimento+=arquivo[i];
					i++;
				}	 
			} else if (contador == 3) {
				//Extraindo nivel de cor
				while(arquivo[i]!='\n'){
					nivel_de_cor+=arquivo[i];
					i++;
				}
				break;	
			} 
		}
	}
	
	//Passando dados para os atributos
	this->numero_magico = numero_magico;
	this->largura = stoi(largura);
	this->comprimento = stoi(comprimento);
	this->nivel_de_cor = stoi(nivel_de_cor);
	this->tamanho_cabecalho = i;

}
