#include "ppm.hpp"

//Construtor/Destrutor
PPM::PPM(){
	setArquivo("");
	setNumeroMagico("");
	setEndereco("");
	setLargura(0);
	setComprimento(0);
	setNivelDeCor(0);
	setTamanhoTotal(0);
	setTamanhoImagem(0);
	setTamanhoCabecalho(0);
}

PPM::PPM(string endereco) {
	setArquivo("");
	setNumeroMagico("");
	setEndereco(endereco);
	setLargura(0);
	setComprimento(0);
	setNivelDeCor(0);
	setTamanhoTotal(0);
	setTamanhoImagem(0);
	setTamanhoCabecalho(0);	
}

PPM::~PPM() {

}

//Outros
void PPM::VerificaNumeroMagico() {         // Verifica se a imagem é realmente uma PPM
	if (getNumeroMagico() == "P6") {
		//Tá tudo ok!
	}else if (getNumeroMagico() == "P5") {
		cout << "Ops, parece que você nos deu uma imagem do tipo '.pgm' na opção da '.ppm'!" << endl;
		exit(0);
	} else {
		cout << "Ops, parece que você nos deu uma imagem diferente do esperado!" << endl;
		exit(0);
	}
}

void PPM::CalculaTamanhoImagem() {
	setTamanhoImagem(getLargura()*getComprimento()*3);
}

void PPM::DecifraImagem() {
	size_t tamanho_endereco = getEndereco().size();              // Defini o tamanho do endereço
	int tipo_filtro;                                             // Defini qual filtro deve ser aplicado  
	int mais_filtros = 1;                                        // Defini se o usuário quer apicar outro filtro novamente
	int inicio = getTamanhoCabecalho();                          // Definindo inicio da imagem, retirando o cabeçalho
	int i = 0;                                                   // Contador do aplicador do filtro
	int contador_ciclos = 0;                                     // Conta 3 ciclos e depois zera

	// Aplicando os filtros de acordo com a escolha
	while (mais_filtros == 1) {

		cout << "Digite 1 - Para aplicar o filtro R 'Red';" << endl << "Digite 2 - Para aplicar o filtro G 'Green';" << endl << "Digite 3 - Para aplicar o filtro B 'Blue';" << endl << endl;
		cout << "Insira o número correspondente ao filtro que deseja aplicar de acordo com as opções dadas: ";
		cin >> tipo_filtro;
		cout << endl;

		if (tipo_filtro == 1) {		//FILTRO VERMELHO		

			// Trocando o nome da imagem
			string novo_nome_r;

			for(size_t j = 0;j < tamanho_endereco - 4;j++) {
				novo_nome_r+=getEndereco()[j];
			}

			novo_nome_r+="_RED.ppm";

			// Criando nova imagem
			ofstream imagem_red;
			imagem_red.open(novo_nome_r.c_str());
			string arquivo = getArquivo();

			// Aplicando o filtro RED
			for (i = 0; i <= inicio; i++) {
				imagem_red << arquivo[i];
			}
			for (i = i;i <= getTamanhoTotal(); i++) {
				contador_ciclos++;
				if (contador_ciclos == 1) {
					imagem_red << arquivo[i];		
				} else if (contador_ciclos == 2) {
					imagem_red << 0;
				} else {
					imagem_red << 0;
					contador_ciclos = 0;
				}
			}
			
			// Limpar tudo
			novo_nome_r.clear();
			arquivo.clear();
			tipo_filtro = 0;
			i = 0;
			contador_ciclos = 0;

			// Fechando Imagem
			imagem_red.close();

			cout << "Sua imagem está pronta!" << endl << "Vá até a pasta de origem da imagem e verifique se há uma mensagem secreta!" << endl;

		} else if (tipo_filtro == 2) {		//FILTRO VERDE			

			// Trocando o nome da imagem
			string novo_nome_g;

			for(size_t j = 0;j < tamanho_endereco - 4;j++) {
				novo_nome_g+=getEndereco()[j];
			}

			novo_nome_g+="_GREEN.ppm";

			// Criando nova imagem
			ofstream imagem_green;
			imagem_green.open(novo_nome_g.c_str());
			string arquivo = getArquivo();

			// Aplicando o filtro RED
			for (i = 0; i <= inicio; i++) {
				imagem_green << arquivo[i];
			}
			for (i = i;i <= getTamanhoTotal(); i++) {
				contador_ciclos++;
				if (contador_ciclos == 1) {
					imagem_green << 0;
				} else if (contador_ciclos == 2) {
					imagem_green << arquivo[i];
				} else {
					imagem_green << 0;
					contador_ciclos = 0;
				}
			}
			
			// Limpar tudo
			novo_nome_g.clear();
			arquivo.clear();
			tipo_filtro = 0;
			i = 0;
			contador_ciclos = 0;

			// Fechando Imagem
			imagem_green.close();

    		// Escolhendo se quer mais filtros
			cout << "Sua imagem está pronta!" << endl << "Vá até a pasta de origem da imagem e verifique se há uma mensagem secreta!" << endl;

		}else if (tipo_filtro == 3) {		// FILTRO AZUL			

			// Trocando o nome da imagem
			string novo_nome_b;

			for(size_t j = 0;j < tamanho_endereco - 4;j++) {
				novo_nome_b+=getEndereco()[j];
			}

			novo_nome_b+="_BLUE.ppm";

			// Criando nova imagem
			ofstream imagem_blue;
			imagem_blue.open(novo_nome_b.c_str());
			string arquivo = getArquivo();

			// Aplicando o filtro RED
			for (i = 0; i <= inicio; i++) {
				imagem_blue << arquivo[i];
			}
			for (i = i;i <= getTamanhoTotal(); i++) {
				contador_ciclos++;
				if (contador_ciclos == 1) {
					imagem_blue << 0;
				} else if (contador_ciclos == 2) {
					imagem_blue << 0;
				} else {
					imagem_blue << arquivo[i];
					contador_ciclos = 0;
				}
			} 
			
			// Limpar tudo
			novo_nome_b.clear();
			arquivo.clear();
			tipo_filtro = 0;
			i = 0;
			contador_ciclos = 0;

			// Fechando Imagem
			imagem_blue.close();

    		// Escolhendo se quer mais filtros
			cout << "Sua imagem está pronta!" << endl << "Vá até a pasta de origem da imagem e verifique se há uma mensagem secreta!" << endl;

		} else {
			cout << "Oops, parece que você quer aplicar um filtro fora das nossas opções!" << endl << endl;
		}

		cout << "Deseja aplicar mais um filtro?   (Digite - 1 para sim / Digite - 0 para não): ";
		cin >> mais_filtros;
		cout << endl;

	}

	cout << "Adeus, Obrigado!" << endl << endl;
}