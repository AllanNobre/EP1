#include "imagem.hpp"
#include "pgm.hpp"
#include "ppm.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

using namespace std;

int main(int argc, char ** argv) {

	string endereco;
	int tipo_imagem = 0;

	cout << endl << "Seja Bem Vindo" << endl;
	cout << endl << "Digite 1 - Para Imagens '.pgm';" << endl << "Digite 2 - Para Imagens '.ppm';" << endl << "Digite 3 - Para sair;" << endl << endl;
	cout << "Insira o número correspondente ao tipo de imagem que deseja utilizar de acordo com as opções dadas: ";
	cin >> tipo_imagem;
	cout << endl;

	while(tipo_imagem!=1 && tipo_imagem!=2 && tipo_imagem!=3) {
		cout << "Número inválido, digite um dos números informados: " << endl;
		cin >> tipo_imagem;
		cout << endl;
	}

	if (tipo_imagem==3) {
		cout << "Adeus, Obrigado!" << endl << endl;
		exit(0);
	} else if (tipo_imagem==2)	{
		cout << "Insira o endereço(caminho) da imagem que deseja utilizar - 'Exemplo: ./doc/segredo.ppm' - : ";
		cin >> endereco;
		cout << endl;

		//Aqui se cria o objeto PPM:
		PPM *imagem;
		imagem = new PPM(endereco);

		imagem->AbrirImagem();
		imagem->ExtrairDados();
		imagem->VerificaNumeroMagico();
		imagem->CalculaTamanhoImagem();
		imagem->DecifraImagem();

		delete (imagem);

	} else {
		cout << "Insira o endereço(caminho) da imagem que deseja utilizar - 'Exemplo: ./doc/lena.pgm' - :  ";
		cin >> endereco;
		cout << endl;

		//Aqui se cria o objeto PGM:
		PGM *imagem;
		imagem = new PGM(endereco);

		imagem->AbrirImagem();
		imagem->ExtrairDados();
		imagem->VerificaNumeroMagico();
		imagem->CalculaTamanhoImagem();
		imagem->ExtrairPosicaoMensagem();
		imagem->DecifraImagem();

		delete (imagem);

	}

	return 0;
}