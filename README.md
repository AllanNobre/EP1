-> Para executar esse programa (utilizou-se o arquivo MAKEFILE) digite:


make - "Para compilar os aquivos";

make run - "Para executar o binário criado";


Após esses passos, siga as instruções que o programa der para poder utilizá-lo de forma correta.


Exemplos de caminhos da imagem:


./doc/Lena.pgm - "Para utilizar a imagem Lena.pgm que está na pasta 'doc'";

./doc/segredo.ppm - "Para utilizar a imagem segredo.ppm que está na pasta 'doc'";


Ao termino do programa, digite:


make clean: "Para limpar os arquivos da pasta bin e obj";


-> Informações adicionais:


bin: "Pasta que mantem o binário";

doc: "Pasta onde contem qualquer informação extra de documentação";

inc: "Pasta que mantém os headers";

obj: "​Pasta que receberá os arquivos objetos";

src: "Pasta que mantém as implementações dos hearders";


As mensagens escondidas nas imagens do tipo .pgm serão apresentadas no terminal.


As imagens com os filtros RGB, serão salvas na pasta de origem da imagem original com o nome do arquivo oiginal acrescido de '_(Cor do filtro)'.


Exemplo:


NomeDaImagemDeOrigem_RED.ppm - "Para o filtro RED aplicado";

NomeDaImagemDeOrigem_GREEN.ppm - "Para o filtro GREEN aplicado";

NomeDaImagemDeOrigem_BLUE.ppm - "Para o filtro BLUE aplicado";


Tenha um bom uso, Obrigado.

