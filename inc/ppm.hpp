#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <cstring>

using namespace std;

class PPM : public Imagem {
	public:
		//Construtor/DEstrutor
		PPM();
		PPM(string endereco);
		~PPM();
		//Outros
		void VerificaNumeroMagico();
		void CalculaTamanhoImagem();
		void DecifraImagem();

};

#endif