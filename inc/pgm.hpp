#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

using namespace std;

class PGM : public Imagem {
	private:
		int posicao_mensagem;

	public:
		//Construtor/DEstrutor
		PGM();
		PGM(string endereco);
		~PGM();
		//Acessores
		void setPosicaoMensagem(int posicao_mensagem);
		int getPosicaoMensagem();
		//Outros
		void ExtrairPosicaoMensagem();
		void VerificaNumeroMagico();
		void CalculaTamanhoImagem();
		void DecifraImagem();

};

#endif