#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

using namespace std;

class Imagem {
	private:
		string arquivo;
		string numero_magico;
		string endereco;
		int largura;
		int comprimento;
		int nivel_de_cor;
		int tamanho_total;
		int tamanho_imagem;
		int tamanho_cabecalho;

	public:
		//Construtor/Destrutor:
		Imagem();
		virtual ~Imagem();
		//Acessores:
		void setArquivo(string arquivo);
		string getArquivo();
		void setNumeroMagico(string numero_magico);
		string getNumeroMagico();
		void setEndereco(string endereco);
		string getEndereco();
		void setLargura(int largura);
		int getLargura();
		void setComprimento(int comprimento);
		int getComprimento();
		void setNivelDeCor(int nivel_de_cor);
		int getNivelDeCor();
		void setTamanhoTotal(int tamanho_total);
		int getTamanhoTotal();
		void setTamanhoImagem(int tamanho_imagem);
		int getTamanhoImagem();
		void setTamanhoCabecalho(int tamanho_cabecalho);
		int getTamanhoCabecalho();
		//Outros:
		void AbrirImagem();
		void ExtrairDados();
		virtual void VerificaNumeroMagico() = 0;
		virtual void CalculaTamanhoImagem() = 0;
		virtual void DecifraImagem() = 0;

};

#endif 